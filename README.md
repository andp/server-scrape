small function to scrape all history from discord servers + channels

# use

import the module then run `discordScrape({config})`. where config =
```
{
    serverChannelList: {
        'SERVER_ID': ['CHANNEL_ID']
    },
    discordToken: '123',
    exportPath: 'somehwere'
}
```

`discordToken` can either be passed in or set in a `.env` file as `DISCORD_TOKEN`

`exportPath` is optional and defaults to `./logs`

# etc

a subdirectory for each server will be created in the output directory with each channel receiving its own log file.
Maybe there'll be discord category sub directory support in the future

# issues

 * TODO :: handle insufficient permission to view - doesn't handle error 
 * TODO :: doesn't get very latest message
 * TODO :: parse embeds - this will return as a blank message
 * TODO :: reverse order before writing? - currently the latest message is at top of file which can make reading them a little weird
