const scrapeInit = require('./modules/serverScrape');

/**
 * scrape discord
 * @param {Object} config
 * @param {Object} config.serverChannelList - list of servers with channels to scrape
 * @param {string} [config.discordToken=process.env.DISCORD_TOKEN] - required for bot to run
 * @param {string} [config.exportPath=./logs] - Somebody's name.
 *
 * {
 *   serverChannelList: {
 *     '123': ['567', '890]
 *   },
 *   discordToken: '123',
 *   exportPath: '../somewhere'
 * }
 */
exports.discordScrape = ({config}) => {
  return scrapeInit({config}).then(() => console.log('finished'));
}
