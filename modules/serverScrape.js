const Discord = require('discord.js');
const fs = require('fs');
const bot = new Discord.Client();

/**
 * TODO :: handle insufficient permission to view
 * TODO :: doesn't get very latest message
 * TODO :: parse embeds
 * TODO :: reverse order before writing?
 */

const getUsername = async userId => {
  const user = await bot.users.get(userId);
  return user && user.username || '';
};

const fixUsernames = async (content, matches) => {
  let messageContent = content;
  await Promise.all(matches.map(async match => {
    const userString = match;
    const resolvedUser = await getUsername(match.replace(/[<>@!]/g, ''));
    messageContent = messageContent.replace(userString, resolvedUser);
  }));
  return messageContent;
};

const recurseMessages = async (bot, messageId, channel, messageArray = []) => {
  console.log(`${channel} - messageArray.length: ${messageArray.length}`);
  const messages = await bot.channels.get(channel).fetchMessages({
    limit: 100,
    before: messageId
  });
  messageArray.push(...await Promise.all(messages.map(async message => {
    let content = message.content;
    const matches = content.match(/<@!?\S*>/g);
    if(matches && matches.length > 0) {
      content = await fixUsernames(content, matches);
    }
    content = content.replace(/<a?:\S*>/g, '');
    const messageAuthor = await getUsername(message.author.id);
    const attachmentArray = [];
    if(message.attachments.array().length)
      message.attachments.array().map(attachment => attachmentArray.push(attachment.url));
    return `[${message.createdAt}] ${messageAuthor}: ${content} ${attachmentArray.length ? attachmentArray.join(', ') : ''}`;
  })));
  if(!messages.last()) return messageArray;
  else return recurseMessages(bot, messages.last().id, channel, messageArray);
};

const scrapeInit = ({config}) => {
  const {
    serverChannelList,
    discordToken = process.env.DISCORD_TOKEN,
    exportPath = './logs'
  } = config;
  bot.on('ready', async () => {
    console.info('starting');
    fs.mkdirSync(exportPath, { recursive: true });
    await Promise.all(Object.entries(serverChannelList).map(async ([serverId, channelList]) => {
      console.log(`serverId: ${serverId}`);

      const { name: serverName } = await bot.guilds.get(serverId);
      console.log(serverName);

      const normalisedServerName = serverName.replace(/\s/g, '');
      console.log(normalisedServerName);

      const directoryPath = `${exportPath}/${normalisedServerName}`;
      fs.mkdirSync(directoryPath, { recursive: true });

      await Promise.all(channelList.map(async channelId => {
        const { name: channelName } = await bot.channels.get(channelId);
        console.log(channelName);
        const messages = await bot.channels.get(channelId).fetchMessages({
          limit: 1
        });
        const messageArray = await recurseMessages(bot, messages.last().id, channelId);
        const fullContent = messageArray.join('\n');
        const fileName = `file_${channelId}_${channelName}.txt`;
        console.log(fileName);
        fs.writeFileSync(`${directoryPath}/${fileName}`, fullContent);
        console.log("File has been saved.");
        return null;
      }));
    }));
    process.exit();
  });
  return bot.login(discordToken);
};

module.exports = scrapeInit;
